from rake_nltk import Rake
import time
import re
import RAKE
import requests
import traceback
import uuid
import os
import re
import json
import urlparse
import json
from models import *
from django.views.generic import View
from django.core.files.storage import default_storage
from django.conf import settings
from django.core.files.base import ContentFile
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect
import random
import string
from django.db import transaction
from django.shortcuts import render, redirect
import haikunator
from .models import Room
import urllib2
import speech_recognition as sr
import subprocess
import os
import urllib
import hashlib
import os
import json
import datetime
import requests
import redis
import logging
logger = logging.getLogger(__name__)

r = redis.Redis(host='localhost', port=6379, db=0)

def about(request):
	return render(request, "chat/about.html")

def getKeys(request):
	keys = r.keys(pattern='*')
	mainKeys = []
	for key in keys:
		if 'asgi:' not in key:
			mainKeys.append(key)
	data = {"data":[]}
	for mainKey in mainKeys:
		value = r.get(mainKey)
		data['data'].append({'key':mainKey,'value':value})

	return HttpResponse(json.dumps(data), content_type='application/json')

def checkBotCall(request):
	if r.get(request['QUERY_STRING']) != None:
		val = json.loads(r.get(request['QUERY_STRING']))
		print 'value ==>' ,val
		if val['classification'] == True:
			return True
		return False
	return False

def dashboard(request):
	return render(request, "chat/room.html", {
	})

def test(request):
	epoch_time = int(time.time())
	filterLog(request)
	populateLog(request)
	request.META['epoch_time'] = epoch_time
	fileData = {
		'request':request.META
	}
	logger.info(json.dumps(fileData))
	if(checkBotCall(request.META)):
		return HttpResponse(json.dumps({'error':'FORBIDDEN'}),content_type='application/json');    
	
	return HttpResponse(json.dumps(fileData),content_type='application/json');

def filterLog(request):
	queryString = request.META['QUERY_STRING']
	request.META.clear()
	request.META['QUERY_STRING'] = queryString

def populateLog(request):
	request.META['DeploymentId'] = id_generator()
	request.META['AccountId'] = '313316836846'
	request.META['Operation'] = 'InvokeGet'
	request.META['RestapiId'] = '0omasvr1c4'
	request.META['StatusCode'] = 200
	request.META['XForwardedForHeader'] = '54.240.198.12'
	request.META['AwsUserPrincipal'] = '*******'
	request.META['AwsCallerPrincipal'] = '******'
	request.META['BackplaneRequestId'] = 'TqXf4EAuSK4FkEQ='
	request.META['UserAgent'] = 'Java/1.8.0_181'
	request.META['EndpointType'] = 'REGIONAL'
	request.META['ThrottleDeciderRequestId'] = 'TqXf4PhyyK4Gr7g='
	request.META['IntegrationType'] = 'MOCK'
	request.META['InvokingPath'] = '/v1/iam-auth'
	request.META['ApiKeyId'] = 'INVALID'
	request.META['RemoteSocket'] = '10.0.56.175:16380'
	request.META['RequestId'] = '5891d822-1a8b-11e9-967e-d9bd57a9da63'
	request.META['InstanceId'] = 'i-0cc6612998cd26463'
	request.META['StageName'] = 'v1'
	request.META['AvailabilityZone'] = 'us-west-1c'
	request.META['PID'] = '32404@ip-10-0-100-17'
	request.META['Host'] = '0omasvr1c4.execute-api.us-west-1.amazonaws.com'
	request.META['Method'] = 'GET'
	request.META['CachedResult'] = 'false'
	request.META['Marketplace'] = 'us-west-1'
	request.META['AwsAccountId'] = '313316836846'
	request.META['AwsUserArn'] = 'arn:aws:iam::313316836846:user/canary'
	request.META['CanaryQueryString'] = 'canary-request-id=8ff018bf-b1fb-4826-ae83-b9db7cc2285c'
	request.META['AwsAccessKeyId'] = 'AKIAI55RTK352V6H37VA'
	request.META['Size'] = 28
	request.META['Time'] = '7.053732 ms'
	request.META['EndTime'] = 'Thu, 17 Jan 2019 19:09:00 UTC'
	request.META['StartTime'] = '1547752140.024'
	request.META['Program'] = 'BackplaneExecutionService'
	request.META['Timing'] = 'CloudWatchPostTime:0/1,AuthClient:DerefNamedPoliciesTimer:0.357617/1,ThrottleTime:0.037204/1,CloudWatchBeanGetTime:0/1,MethodResponseRegexMatchTime:0/1, UpdateInvokeTimeHandler:0/1,CreateRequestLatency:0/1,BuildMethodResultTime:1/1,AccessLogWriteTime:0/1,LabelAndDeploymentConfigGetTime:0/1,AuthTypeHandlerBefore:0/1, ContinuationFinalizeTime:0/1,RouteResolutionTime:0/1,TransformVelocityEvaluateActualTime:1/2,GenerateResourceArn:0.004038/1,CheckThrottleAndQuotaOverhead:2/1,TotalTime:7.304591/1,  TransformJsonParseTime:0/2,MaxRequestsQueueTime:0.001184/1,ThrottleDeciderOverheadLatency:2/1,TimeSinceLastManifestProcessed:4082/1,QueueTime:0.031494/1, GetMethodResultBodyLatency:1/1,BackplaneLatencyUnderGoal:7/1,LogInvokeRequestLatency:0/1,AuthClient:Authenticate:2.994794/1,BackplaneMinusDNSLatency:2/2,AuthClient:AuthenticateUser:2.994794/1,OverallBackplaneLatency:7/1,TicketHolderOverheadLatency:0/1,MethodOverheadTime:0/1,FindOutputMappingLatency:0/1,TransformingBodyLatency:1/1,CreateResponseTime:0/1,AuthClient:EvaluateRequest:0.102272/11,AuthClient:AuthorizeAction:0.127414/1,ProcessContinuationResponseLatency:1/1,FindMethodResponseLatency:0/1,BackplaneLatency:2/2,CreateMethodResultLatency:1/1,GetOutputMappingTemplatesLatency:0/1,ActivityTime:0.735597/1,TransformVelocityEvaluateTime:1/2,LogInvokeResponseLatency:0/1'
	request.META['Counters'] = 'TooManyActiveApiKeysTD=0,LoadShed=0,UpdateInvokeTimeHandlerFaultCount=0,UsagePlansAPIKeyQuotaExceeded=0,AwsSignature_Version4=1,GetDeploymentConfigUnhandledExceptionCount=0,ChainPostprocessingError=0,VelocitySafeguardExceptionCount=0,NumberOfMethods=23,Fault=0,AuthClient:DerefCachedNamedPolicies=7,TooManyActiveApiKeysTH=0,VelocityFatalCount=0,Throttle=0,MultiValueRequestHeaderCount=0,BasePathMappingConfigNotFoundCount=0,UnhandledAuthErrorCount=0,CacheRefresherActiveThreadCount=0,MultiValueResponseHeaderCount=0,EndpointTimeout=0,Failure=0,TransformOutputTooLarge=0,MethodConfigNotFoundCount=0,ApiOwnerErrorCount=0,MultiValueRequestQueryStringCount=0,AccessLoggingHandlerErrorCount=0,InFlight=12,UsagePlansAPIKeyThrottled=0,RestApiIdNotFoundCount=0,AuthClient:AuthenticateSuccess=1,GetDeploymentConfigExecutionExceptionCount=0,MethodBackplaneErrorCount=0,MethodSuccessCount=1,UsagePlansAPIKeyForbidden=0,CannotRecognizeHostName=0,ThrottleByMethod=0,AuthClient:AuthorizePolicies=11,ChainPreprocessingFault=0,CoralThrottleVersion1ThrottleCount=0,VelocityUnhandledErrorCount=0,CustomerCacheMissCount=0,AuthClient:DerefNamedPoliciesCounter=7,RestApiNotLoadedErrorCount=0,MaxRequestsQueueSize=0,UpdateInvokeTimeHandlerCacheSize=1169,CacheRefresherBlockingQueueSize=0,MeterFailure=0,CapacityUsed=0,VelocityOutOfMemoryErrorCount=0,UsagePlansQuotaServiceSuccessCount=1,MethodClientErrorCount=0,ThrottleByHostname=0,ThrottleByApiKey=0,protocol_HttpBinding=1,UsagePlansAPIKeyFailure=0,MeteringHandlerUnknownThrowableCount=0,TransformVelocityTimeout=0,ThrottleDeciderServiceSuccessCount=1,UnknownInternalErrorCount=0,ChainPreprocessingError=0,ThrottledByAuthorizerThrottleKey=0,MethodRequestBytes=0,MethodResponseBytes=28,StatusCode200Count=1,CacheInvalidationRequestCount=0,ThrottleDeciderShardCount=0,RestApiConfigDaysOld=7,Error=0,GetDeploymentConfigS3ExceptionCount=0,UsagePlansNotFound=0,GetDeploymentConfigNotFoundCount=0,ApiNegativeCacheHit=0,CacheRefresherThreadPoolSize=50,AuthClient:NamedPolicyCacheHitAll=1,UsagePlansAPIKeyFault=0,MockRequestCount=1,RestApiConfigCacheMissCount=0,CacheMissCount=0,ChainPostprocessingFault=0,EndpointCallFailure=0,UsagePlansFinalizeError=0,ThrottleByAccount=0,DataTransferOutBytes=65,AuthClient:AuthenticateCacheHit=1,FinalizeError=0'
	request.META['Levels'] = 'DeploymentCacheHitRate=99.94497/1 %,ThrottlingProximity=0.016667/1,DeploymentCacheEvictionCount=0/1,DeploymentCacheMissRate=0.05503/1 %,DeploymentCacheHitCount=67442250/1,   DeploymentCacheMissCount=37134/1'
	request.META['Metrics'] = 'BackplaneNormalizedLatency=0.035714*28 IntegrationType|MOCK,AuthType:AWS_IAM=1 IntegrationType|MOCK,DNSResolveTime=0 ms IntegrationType|MOCK,IntegrationLatency=0 ms IntegrationType|MOCK,IntegrationEndpointErrorCount=0 IntegrationType|MOCK'


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
	return ''.join(random.choice(chars) for _ in range(size))

def new_room(request):
	"""
	Randomly create a new room, and redirect to it.
	"""
	new_room = None
	while not new_room:
		with transaction.atomic():
			label = haikunator.haikunate()
			if Room.objects.filter(label=label).exists():
				continue
			new_room = Room.objects.create(label=label)
	return redirect(chat_room, label=label)

def chat_room(request, label):
	"""
	Room view - show the room, with latest messages.

	The template for this view has the WebSocket business to send and stream
	messages, so see the template for where the magic happens.
	"""
	# If the room with the given label doesn't exist, automatically create it
	# upon first visit (a la etherpad).
	room, created = Room.objects.get_or_create(label=label)

	# We want to show the last 50 messages, ordered most-recent-last
	messages = reversed(room.messages.order_by('-timestamp')[:50])

	return render(request, "chat/room.html", {
		'room': room,
		'messages': messages,
	})

def convert_date(s):
	try: 
		return int(datetime.datetime.strptime(s, '%Y-%m-%d').strftime("%s"))*1000
	except ValueError:
		return False

def convert_int(s):
	try:
		return int(s)
	except ValueError:
		return None

def convert_bool(s):
	try:
		s = s.lower()
		if s == 'true' or s == 'on':
			return True
		elif s == 'false' or s == 'off':
			return False
	except ValueError:
		return None

def convert_value(attribute, value):
	try:
		value = str(value)
		value_int = convert_int(value)
		value_bool = convert_bool(value)
		value_date = convert_date(value)
		if value_int or value_int == 0:
			value = value_int
		if value_bool is not None:
			value = value_bool
		if value_date:
			value = value_date
		return value
	except Exception as e:
		return value

def clean_data(data):
	try:
		data = dict(data.iterlists())
		data = get_single(data)
		for d in data:
			if type(data[d]) == list:
				for i,a in enumerate(data[d]):
					data[d][i] = convert_value(d, data[d][i])
			else:
				data[d] = convert_value(d, data[d])
		return data
	except Exception as e:
		return data

def get_single(keys):
	res = {}
	for key in keys:
		if len(keys[key]) == 1:
			if ',' in keys[key][0]:
				res[key] = keys[key][0].split(',')
			else:
				res[key] = keys[key][0]
		else:
			res[key] = keys[key]
	return res

def uploadAudioFile(request, *args, **kwargs):
	print 'testing';
	options = {
		'error': 'only post request configured over this endpoint'
	}
	print request.POST;
	if not request.POST:
		return HttpResponse(json.dumps(options),content_type='application/json');
	deal = {
		'testing':'hello world'
	}
	print request.POST
	attributes = clean_data(request.POST)
	print attributes
	save_and_upload(request, attributes)
	options = get_single(
				json.loads(json.dumps(urlparse.parse_qs(request.META['QUERY_STRING']))))

	return HttpResponse(json.dumps(options),content_type='application/json');

def save_and_upload(request, attributes):
	try:
		for f in request.FILES:
			unique_id = uuid.uuid4()
			data = request.FILES[f]
			loc = 'tmp/'+ str(unique_id) + '.wav'
			path = default_storage.save(
				loc, ContentFile(data.read()))
			tmp_file = os.path.join(settings.MEDIA_ROOT, path)
			print tmp_file
			print 'Converting Text to speech '
			r = sr.Recognizer()
			with sr.AudioFile(tmp_file) as source:
				r.adjust_for_ambient_noise(source)
				audio = r.record(source)
			command = r.recognize_google(audio)
			print command
			options = {}
			options['path'] = tmp_file
			options['text'] = command
			if 'callId' in attributes:
				options['callId'] = attributes['callId']
			saveNewCallIntoDB(options);
	except Exception as e:
		traceback.print_exc()

def createCall():
	unique_id = uuid.uuid4()
	print str(unique_id)
	call = Call(call_id=str(unique_id))
	call.save()
	return call

def createFile(call, path, text):
	filee = File(name=path,text=text,callId=call)
	filee.save()
	return filee

def saveNewCallIntoDB(options):
	print options
	if 'callId' not in options:
		call = createCall()
		return createFile(call, options['path'], options['text'])
	else:
		callId = options['callId']
		call = Call.objects.get(call_id=callId)
		return createFile(call, options['path'], options['text'])


def get_objects(objs, flag=0):
	if flag == 0:
		return [obj.get_json() for obj in objs ]
	else:
		return [obj.get_json(flag=flag) for obj in objs]

def getCallById(request, callId):
	options = {
		'callId':callId
	}
	call = Call.objects.filter(call_id=callId);
	print '###' , call;
	fileData = {}
	if(len(call) > 0):
		id = call[0].id
		print id
		fileData = get_objects(File.objects.filter(callId_id=id));
		print fileData
	return HttpResponse(json.dumps(fileData),content_type='application/json');

def callPunchuatedText(request, callId):
	options = {
		'callId':callId
	}
	call = Call.objects.filter(call_id=callId);
	print '###' , call;
	fileData = {}
	if(len(call) > 0):
		id = call[0].id
		print id
		fileData = get_objects(File.objects.filter(callId_id=id));
		text = ''
		for f in fileData:
			text = f['text'] + text + ' ';
		print 'Getting punchuated text :'
		r = requests.post("http://bark.phon.ioc.ee/punctuator", 
			data={'text': text});
		print 'Received punchuated text from punctuator service .'
		text = r.content;
		
		xx = RAKE.Rake(RAKE.SmartStopList())
		keywords = xx.run(text,minCharacters = 1, maxWords = 1, minFrequency = 1)[0:5]

		ra = Rake()
		ra.extract_keywords_from_text(text)
		phrases = ra.get_ranked_phrases()[0:5]
		print phrases
		for phrase in phrases:
			boldKeyword = '<b>' + phrase + '</b>'
			insensitive_hippo = re.compile(re.escape([phrase][0]), re.IGNORECASE)
			text = insensitive_hippo.sub(boldKeyword,text)

		words = ['wifi', 'bluetooth', 'device', 'alexa']
		for w in words:
			boldKeyword = '<b>' + w + '</b>'
			insensitive_hippo = re.compile(re.escape(w), re.IGNORECASE)
			text = insensitive_hippo.sub(boldKeyword,text)

		response = {
			'text':text,
			'keywords':keywords
		}
	return HttpResponse(json.dumps(response),content_type='application/json');


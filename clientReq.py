import traceback
import requests
import time
import sys
import grequests

total = len(sys.argv)
name = str(sys.argv[1])
degree = int(str(sys.argv[2]))

import random

def generate_polynomial(degree, a, b):
    """chooses coefficients for a polynomial of the given degree, such that f(a) == b"""

    #to fit only one data point, we can choose arbitrary values for every coefficient except one, which we initially set to zero.
    coefficients = [0] + [1 for _ in range(degree-1)]

    #now calculate f(a). This will probably not be equal to b, initially.
    y = sum(coefficient * a**n for n, coefficient in enumerate(coefficients))

    #setting the final coefficient to their difference will cause f(a) to equal b.
    coefficients[0] = b - y

    return coefficients

seq = generate_polynomial(degree, 1, 1)
print(seq)

def getFunctionValue(x):
	value = 0
	for element in seq:
		value = value + (1*x)
		x = x*x
	return value

print 'Client name: ' , name
counter = 0
while counter < 200:
	counter = counter + 1
	try:
		URL = "http://localhost:8000/test?name=" + name
		value = getFunctionValue(counter)
		print 'Making ' + str(value) + ' requests'
		if value > 50:
			counter = 1
			continue
		rs = (grequests.get(URL) for u in range(0,value))
		print grequests.map(rs)
	except Exception as e:
		print(traceback.format_exc())
		continue
	print 'Sleeping for 1 second'
	time.sleep(1)


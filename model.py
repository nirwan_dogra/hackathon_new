import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

from sklearn import linear_model
from sklearn import svm

classifiers = [
    # svm.SVR(),
    # linear_model.SGDRegressor(),
    linear_model.BayesianRidge(),
    # linear_model.LassoLars(),
    # linear_model.ARDRegression(),
    # linear_model.PassiveAggressiveRegressor(),
    # linear_model.TheilSenRegressor(),
    # linear_model.LinearRegression()
    ]

data = pd.read_csv('model3.csv', delimiter=",")

label = data['label']
features = data.drop('label', axis=1)


regr = linear_model.BayesianRidge()
regr.fit(features, label)

series = [4,6,13,18,8,9,16,15,14,13,8,4,11,10,9,8,7,6,5,4,1,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
]
print 'prediction =>' , regr.predict([series]).tolist()



# Split the data into training/testing sets
data_X_train = features[:-3000]
data_X_test = features[-3000:]

# Split the targets into training/testing sets
data_y_train = label[:-3000]
data_y_test = label[-3000:]




# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(data_X_train, data_y_train)

# Make predictions using the testing set
data_y_pred = regr.predict(data_X_test)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean squared error
print("Mean squared error: %.2f"
      % mean_squared_error(data_y_test, data_y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % r2_score(data_y_test, data_y_pred))

# Split the data into training/testing sets
#data_X_train = features[:-3869]
#data_X_test = features[-3869:]

# Split the targets into training/testing sets

#data_y_train = label[:-3869]
#data_y_test = label[-3869:]

#clf = RandomForestClassifier()
#trained_model = clf.fit(data_X_train, data_y_train)


#print "Trained model :: ", trained_model
#predictions = trained_model.predict(data_y_pred)
 
#for i in xrange(0, 5):
#    print "Actual outcome :: {} and Predicted outcome :: {}".format(list(data_y_pred)[i], predictions[i])

for item in classifiers:
    print(item)
    clf = item
    clf.fit(data_X_train, data_y_train)
    data_y_pred = clf.predict(data_X_test)
    print("Mean squared error: %.2f"
      % mean_squared_error(data_y_test, data_y_pred))
    print('Variance score: %.2f' % r2_score(data_y_test, data_y_pred))
    print("\n\n")



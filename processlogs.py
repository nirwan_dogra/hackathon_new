#!/usr/bin/env python
import difflib
from scipy import spatial
import time
import numpy
import pika
import redis
import json
import matplotlib.pyplot as plt
from numpy import exp, loadtxt, pi, sqrt
from lmfit import Model

import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.metrics import mean_squared_error, r2_score

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

from sklearn import linear_model
from sklearn import svm

classifiers = [linear_model.BayesianRidge()]

data = pd.read_csv('model3.csv', delimiter=",")

label = data['label']
features = data.drop('label', axis=1)


regr = linear_model.BayesianRidge()
regr.fit(features, label)

# print regr.predict([[4,6,13,18,8,9,16,15,14,13,8,4,11,10,9,8,7,6,5,4,1,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
# ]]).tolist()

# Split the data into training/testing sets
data_X_train = features[:-3000]
data_X_test = features[-3000:]

# Split the targets into training/testing sets
data_y_train = label[:-3000]
data_y_test = label[-3000:]

clf = classifiers[0]
clf.fit(data_X_train, data_y_train)
# xx = [4.2,6,13,18,8,9,16,15,14,13,8,4,11,10,9,8,7,6,5,4,1,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
# ]
# print len(xx)
# data_y_pred = clf.predict([xx])
# print data_y_pred

r = redis.Redis(host='localhost', port=6379, db=0)

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='hello')
channel.queue_purge(queue='hello')

h = {}
data2 = {}

def predictValue(series):
	global clf
	prediction = clf.predict([series[0:60]])
	print 'Prediction for series' , series ,' from BayesianRidge Model: ' ,prediction[0]
	return prediction[0]

def outputToFile(series, label):
	newSeries = series[:]
	if len(newSeries) < 60:
			diff = 61-len(newSeries)
			for x in range(0,diff):
				newSeries.append(0)

	seriesStr = []
	for x in newSeries:
		seriesStr.append(str(x))
	strr = ','.join(seriesStr[0:60])

	strr = strr + ',' + str(label)
	f= open("model3.csv","a")
	f.write(strr + "\n")
	return predictValue(newSeries)


def fitGaussain(y, ip, epoch_time):
	cnt = 0
	x = []
	for r in y:
		x.append(len(y)-1-cnt)
		cnt = cnt + 1
	# print 'Trying to fit the below graph onto gaussian'
	# print x
	# print y

	def gaussian(x, amp, cen, wid):
	    """1-d gaussian: gaussian(x, amp, cen, wid)"""
	    return (amp / (sqrt(2*pi) * wid)) * exp(-(x-cen)**2 / (2*wid**2))


	gmodel = Model(gaussian)
	result = gmodel.fit(y, x=x, amp=5, cen=5, wid=1)

	plt.xlabel('Time (Last ' + str(len(x)) + ' seconds. )')
	plt.ylabel('Request Per Second')
	xi = [len(x)-i-2 for i in range(0, len(x))]
	fig = plt.figure()
	plt.xticks(xi, x)
	plt.plot(x, y, 'bo')
	plt.plot(x, result.init_fit, 'k--')
	plt.plot(x, result.best_fit, 'r-')
	plt.savefig("/Users/nirwand/hackathon_new/static/" + ip.split('=')[1] + ".png")
	plt.close(fig)
	

	score = 0
	try:
		if result.params != None and 'amp' in result.params:
			score = result.params['amp'].correl['cen']
	except Exception as e:
		score = 0
	#print 'Score to fit points on a gaussian model:' , score
	print 'Fitting series on Gaussian Model '
	return score

def isBotRequest(data):
	global data2
	global h
	if r.get(data['QUERY_STRING']) is None:
		r.set(data['QUERY_STRING'], json.dumps({'score': 0,'classification': 'false'}))
	# print data
	ip = data['QUERY_STRING']
	epoch_time = data['epoch_time']
	if(ip not in h):
		h[ip] = {}

	if(epoch_time in h[ip]):
		h[ip][epoch_time] = h[ip][epoch_time] + 1
	else :
		h[ip][epoch_time] = 1

	sum = 0
	for x in h[ip]:
		sum = sum + h[ip][x]

	# print h
	# print sum

	m = 0
	period = 0
	series = []
	maxpoints = 180
	cnt = 0
	prevepoch = 0
	for key in reversed(sorted(h[ip].iterkeys())):
		if prevepoch-key > 5:
			# print 'Updating the Hash Map to Empty'
			# print prevepoch , '  ' , key
			h[ip] = {}
			return
		prevepoch = key
		cnt = cnt + 1
		if cnt > maxpoints:
			break
		series.append(h[ip][key])
	print 'Pattern matching running on series : ' , series

	if len(series) <= 5:
		return False
	cnt = 0
	while len(series) > 5 and cnt<5:
		fitGaussain(series[0:10], ip, epoch_time)
		if checkSimilarElements(series, data):
			print 'Overall Score of series: ' , series , ' ' , data2['score']
			r.set(data['QUERY_STRING'], json.dumps(data2))
			return True
		# print series
		a = numpy.diff(series)
		series = a.tolist()
		cnt = cnt + 1
		# print series

	print 'Overall Score of series: ' , series , ' = ' , data2['score']
	if r.get(data['QUERY_STRING']) is None:
		r.set(data['QUERY_STRING'], json.dumps(data2))
	return False

def checkSimilarElements(series ,data):
	global data2
	a = []
	b = []
	for x in range(0,len(series)/2):
		a.append(series[x])
		b.append(series[len(series)-x-1])

	# print a
	# print b
	sm = difflib.SequenceMatcher(None,a,b)
	# print 'Similarity between the two arrays: ', sm.ratio()

	freq = {}
	for element in series:
		if element not in freq:
			freq[element] = 0
		freq[element] = freq[element] + 1
	cnt = 0
	
	score = getScore(freq, len(series))
	# print 'score of series => ', series ,':', score
	predictValue = outputToFile(series,score+sm.ratio())
	overallscore = max(score+sm.ratio(),predictValue)
	data2 = {
		'score': overallscore,
		'classification': overallscore > 0.8,
		'prediction':predictValue
	}
	return  overallscore > 0.8

def getScore(freq, leng):
	# print freq
	score = float(0)
	for element in freq:
		if freq[element] >= 2:
			score = score + ((float)(freq[element])/(float)(leng))
	return score

def callback(ch, method, properties, body):
	try:
		data = json.loads(body)['request']
		isBotRequest(data)
	except Exception as e:
		print e
		# startconsuming()

channel.basic_consume(callback,
					  queue='hello',
					  no_ack=True)

def startconsuming():
	channel.start_consuming()

print(' [*] Waiting for messages. To exit press CTRL+C')
try:
	startconsuming()
except Exception as e:
	startconsuming()

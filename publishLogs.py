#!/usr/bin/env python
import pika
import time, os
import json

#Set the filename and open the file
filename = '/tmp/servicelogs'
file = open(filename,'r')
#Find the size of the file and move to the end
st_results = os.stat(filename)
st_size = st_results[6]
file.seek(st_size)

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

def publishMessage(line):
	channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=line)
	print(" [x] Sent" , line )

print "Starting to read logs for processing "

while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        time.sleep(1)
        file.seek(where)
    else:
      try:
        publishMessage(line)
      except Exception as e:
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        continue

connection.close()

